(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Expansion for pcap expressions
*)

open Pcap_syntax
open Pcap_syntax_aux
open Names


(** Fill in as much info in a primitive as possible **)

let rec expand_prim
 ((prims, acc) : primitive list * primitive list) : primitive list * primitive list =
   match prims with
   | [] -> ([], acc)
   | prim :: rest ->
       begin
       match prim with
       | ((proto, None, None), Nothing) when proto <> None ->
           Aux.diag_output ("INFO: expand_prim no : " ^ Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^ "\n");
           expand_prim (rest, prim :: acc)

       | ((None, None, Some Proto), Escaped_String proto) ->
           (*NOTE in this case we're rewriting something like "proto \tcp" to
                  "tcp", and it can be argued that we're doing the opposite
                  of "expanding", but this is simply a convenient rewrite to
                  turn "proto \tcp" into a form that other rules can match
                  and process.*)
           let prim' = ((Some (proto_of_string proto), None, None), Nothing) in
           Aux.diag_output ("INFO: expand_prim: " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
             " --> " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim') ^ "\n");
           expand_prim (prim' :: rest, acc)

       | ((proto, None, typ), v) when typ <> Some Proto &&
             proto <> Some Vlan && proto <> Some Mpls ->
           let prim' = ((proto, Some Src_or_dst, typ), v) in
           Aux.diag_output ("INFO: expand_prim: " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
             " --> " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim') ^ "\n");
           expand_prim (prim' :: rest, acc)
       | ((proto, dir, None), v) when proto <> Some Vlan && proto <> Some Mpls ->
           let prim' = ((proto, dir, Some Host), v) in
           Aux.diag_output ("INFO: expand_prim: " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
             " --> " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim') ^ "\n");
           expand_prim (prim' :: rest, acc)
       | ((None, dir, ((Some Host) as typ)), ((MAC_Address _) as v)) ->
           let prim' = ((Some Ether, dir, typ), v) in
           Aux.diag_output ("INFO: expand_prim: " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
             " --> " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim') ^ "\n");
           expand_prim (prim' :: rest, acc)
       | ((None, dir, ((Some Host) as typ)), ((IPv4_Address _) as v))
       | ((None, dir, ((Some Host) as typ)), ((String _) as v))
       | ((None, dir, ((Some Net) as typ)), v) ->
           let prims =
             [((Some Ip, dir, typ), v);
              ((Some Arp, dir, typ), v);
              ((Some Rarp, dir, typ), v)] in
           Aux.diag_output ("INFO: expand_prim: " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
             " --> [" ^
             begin
               List.map (fun prim -> Pcap_syntax_aux.string_of_pcap_expression (Primitive prim)) prims
               |> String.concat "; "
             end ^
             "]" ^ "\n");
           expand_prim (prims @ rest, acc)
       | ((None, dir, ((Some Port_type) as typ)), ((Port _) as v)) ->
           let prims =
             [((Some Tcp, dir, typ), v);
              ((Some Udp, dir, typ), v);
              ((Some Sctp, dir, typ), v)] in
           Aux.diag_output ("INFO: expand_prim: " ^
             Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^
             " --> [" ^
             begin
               List.map (fun prim -> Pcap_syntax_aux.string_of_pcap_expression (Primitive prim)) prims
               |> String.concat "; "
             end ^
             "]" ^ "\n");
           expand_prim (prims @ rest, acc)
(*FIXME more?*)
       | ((_, _, _), _) ->
           (*Nothing else to expand at the head, so move
             to the rest*)
           Aux.diag_output ("INFO: expand_prim no : " ^ Pcap_syntax_aux.string_of_pcap_expression (Primitive prim) ^ "\n");
           expand_prim (rest, prim :: acc)
       end

let rec expand_primitives (e : pcap_expression) : pcap_expression =
  match e with
  | Primitive p ->
      begin
      let ([], prims) = expand_prim ([p], []) in
      match prims with
      | [] -> failwith (Aux.error_output_prefix ^ ": " ^ "Impossible")
      | [p] -> Primitive p
      | _ -> Or (List.map (fun p -> Primitive p) prims)
      end
  | True
  | False
  | Atom _ -> e
  | Not e' -> Not (expand_primitives e')
  | And pes -> And (List.map expand_primitives pes)
  | Or pes -> Or (List.map expand_primitives pes)


(** Propagated primitives through disjunctions **)

(*if P || string || string .. then replicate P;
  do this early when processing a pcap expression.*)
(* FIXME Point out resolution of conflict -- if a machine were called 'ftp-data' for example, since were it not for disj_expand then that string would be treated as a hostname by default*)
let rec disj_expand (e : pcap_expression) : pcap_expression =
  match e with
  | Primitive _
  | True
  | False
  | Atom _ -> e
  | Not e' -> Not (disj_expand e')
  | And pes -> And (List.map disj_expand pes)
  | Or pes -> Or (disj_scan pes)

and disj_scan (es : pcap_expression list) : pcap_expression list =
  match es with
  | []
  | [_] -> es
  | (((Primitive ((proto_opt, dir_opt, typ_opt), v)) as pe1) ::
     ((Primitive ((None, None, None), String s)) as pe2) :: result) ->
       let v' =
         match v with
         | Port _ -> Port s
         (*FIXME there might be other kinds of values, not yet supported.
            also need to get this working "src 192.168.1.1 or 192.168.1.2"*)
         | String _ -> String s
         | _ -> failwith (Aux.error_output_prefix ^ ": " ^ "TODO: disj_scan") in
       let pe2' = (Primitive ((proto_opt, dir_opt, typ_opt), v')) in
       Aux.diag_output ("INFO: disj_scan: " ^
         Pcap_syntax_aux.string_of_pcap_expression pe1 ^
         " :: " ^
         Pcap_syntax_aux.string_of_pcap_expression pe2 ^
         " --> " ^
         Pcap_syntax_aux.string_of_pcap_expression pe2' ^ "\n");
       pe1 :: (disj_scan (pe2' :: result))
  | (Primitive _ :: result)
  | (True :: result)
  | (False :: result)
  | (Atom _ :: result) -> List.hd es :: disj_scan result
  | (Not _ :: result)
  | (And _ :: result) ->
      (disj_expand (List.hd es)) :: disj_scan result
  | (Or _ :: result) ->
      failwith (Aux.error_output_prefix ^ ": " ^ "Disjunctions should have been flattened by now")


(** Implicit clauses **)

(*Possible dependencies of a protocol. Returns list of protocols on which a
 given protocol can run. We only look one layer down, and call this function
 repetitively to derive all possible dependencies for a given protocol (and all
 the way down to the link protocol*)
let deps_of_proto (proto : proto) : proto list =
  match proto with
  | Ether -> []
  | Ip -> [Ether]
  | Ip6 -> [Ether]
  | Arp -> [Ether]
  | Rarp -> [Ether]
  | Tcp -> [Ip; Ip6]
  | Udp -> [Ip; Ip6]
  | Sctp -> [Ip; Ip6]
  (*Even though VLAN and MPLS go over Ethernet we do not spell this out here
    because of a quirk in libpcap:
      $ sudo tcpdump -i en5 -d "ether proto \vlan && vlan 100"
      tcpdump: unknown ether proto 'vlan'
  *)
  | Vlan -> []
  | Mpls -> []
  | Icmp -> [Ip]
  | Icmp6 -> [Ip6]

(*This doesn't actually report the "OSI layer", it simply orders headers relative
  to one another in a pragmatic way.*)
let layer_of_proto (proto : proto) : int =
  match proto with
  | Ether -> 2
  | Ip -> 4
  | Ip6 -> 4
  | Arp -> 4
  | Rarp -> 4
  | Tcp -> 5
  | Udp -> 5
  | Sctp -> 5
  | Vlan -> 3
  | Mpls -> 3
  | Icmp -> 5
  | Icmp6 -> 5

let order_proto_pair (proto1 : proto) (proto2 : proto) : (proto * proto) option =
  let o1 = layer_of_proto proto1 in
  let o2 = layer_of_proto proto2 in
  if o1 < o2 then Some (proto1, proto2)
  else if o2 < o1 then Some (proto2, proto1)
  else None

(*If the two don't have a meet, then return an exception -- e.g., Ip and Arp
  don't meet, but Tcp and Ip meet at Tcp*)
let meet (proto1 : proto) (proto2 : proto) : proto option =
  if proto1 = proto2 then Some proto1
  else
  match order_proto_pair proto1 proto2 with
  | None -> None
  | Some (p1, p2) ->
      begin
        match p1, p2 with
        | Ether, p -> Some p
        | Ip, p when p = Tcp || p = Udp || p = Sctp -> Some p
        | Ip6, p when p = Tcp || p = Udp || p = Sctp -> Some p
        | Vlan, p
        | Mpls, p -> Some p
        | _ -> None
      end

let rec implicit_expand_pe (pe : pcap_expression) : pcap_expression =
  let process_connective (f : pcap_expression list -> pcap_expression)
   (pes : pcap_expression list) : pcap_expression =
    if (Contract.contains_SE_proto_pe pe) then
      List.map implicit_expand_pe pes
      |> f
    else
      f pes
      |> implicit_expand in
  match pe with
  | True
  | False -> pe
(*  | Not pe' -> Not (implicit_expand pe')*)
  | Not pe' ->
    if Contract.contains_SE_proto_pe pe then
      Not (implicit_expand_pe pe')
    else
      implicit_expand pe
  | Or pes -> Or (List.map implicit_expand_pe pes)
  | And pes -> process_connective (fun pes -> And pes) pes
  | Primitive _
  | Atom _ ->
     implicit_expand pe

and implicit_expand (pe : pcap_expression) : pcap_expression =
      begin
        let protos =
          protos_in_pe pe StringSet.empty
          |> protos_of_strings in
        let _ =
          Aux.diag_output ("INFO: implicit_expand: " ^
           Pcap_syntax_aux.string_of_pcap_expression pe ^
           " protos_in_pe: [" ^
           begin
             Names.ProtoSet.fold (fun proto acc ->
               string_of_proto proto :: acc
             ) protos []
             |> String.concat "; "
           end ^
           "]" ^ "\n") in
        let meet = ProtoSet.fold (fun proto acc ->
              match acc with
              | None -> Some proto
              | Some other_proto ->
                  begin
                   match meet proto other_proto with
                   (*FIXME possibly incomplete procedure to find meet -- we
                           might be giving up too easily. Alternatively,
                           this might return the wrong value by arbitrarily
                           picking the other protocol in the event of a non-meet:
                   | None -> Some other_proto*)
                   | None -> None
                   | Some meet -> Some meet
                  end)
            protos None in
        let _ =
          Aux.diag_output ("INFO: implicit_expand: " ^
           Pcap_syntax_aux.string_of_pcap_expression pe ^
           " meet: [" ^
           begin
             match meet with
             | None -> "None"
             | Some meet -> "Some " ^ string_of_proto meet
           end ^
           "]" ^ "\n") in
        match meet with
        | None ->
            if ProtoSet.is_empty protos then
              pe
            else failwith (Aux.error_output_prefix ^ ": " ^ "No meet computed")
        | Some meet ->
            let deps = deps_of_proto meet
             |> List.map (fun proto ->
               set_proto proto empty
               |> set_typ Proto
               |> set_value (Escaped_String (string_of_proto meet))
               |> (fun prim -> Primitive prim)
               |> implicit_expand) in

            let disjuncts = List.map (fun dep -> And [dep; pe]) deps in

            if disjuncts = [] then
              (*This means there are no further dependencies,
                so end with what we have*)
              pe
            else
              begin
                begin
                Aux.diag_output ("INFO: implicit_expand: " ^
                 Pcap_syntax_aux.string_of_pcap_expression pe ^
                 " dependencies: [" ^
                 begin
                   List.map Pcap_syntax_aux.string_of_pcap_expression deps
                   |> String.concat "; "
                 end ^
                 "]" ^ "\n")
                end;
                Or disjuncts
              end
      end

let rec push_neg_pe (pe : pcap_expression) : pcap_expression =
  match pe with
  | Primitive _
  | True
  | False
  | Atom _ -> Not pe
  | Not pe' -> pe'
  | And pes -> Or (List.map push_neg_pe pes)
  | Or pes -> And (List.map push_neg_pe pes)

let rec nnf_pe (pe : pcap_expression) : pcap_expression =
  match pe with
  | Primitive _
  | True
  | False
  | Atom _ -> pe
  | Not pe' -> push_neg_pe pe'
  | And pes -> And (List.map nnf_pe pes)
  | Or pes -> Or (List.map nnf_pe pes)

let nnf_pcap (pe : pcap_expression) : pcap_expression =
  Simplifire.trampoline nnf_pe pe
(*
let rec dnf_expand_pe (pe : pcap_expression) : pcap_expression =
  match pe with
  | Primitive _
  | True
  | False
  | Atom _ -> pe
  | Not pe' -> push_neg_pe pe'
  | And pes -> And (List.map disj_expand pes)
  | Or pes -> Or (List.map dnf_expand_pe pes)
*)
