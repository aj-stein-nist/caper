(*
  Copyright Marelle Leon, January 2023
  Copyright Nik Sultana, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: pretty printing of pcap expressions.
  For example, for the expression "ip6 and tcp port 80",
  the output is pretty-printed like this:
    (
      ether proto \ip && ip proto \tcp && ip6 &&
      tcp src or dst port 80
    ) ||
    (
      ether proto \ip6 && ip6 proto \tcp &&
      tcp src or dst port 80
    )


  How this works:
  1) split innermost junction (&&'s or ||'s) into lines of length at most 7 elements.
  2) split outer junctions with lines.
  3) keep track of depth for indentation.
*)


open Pcap_syntax

(*
split_sum split a list into lists of items whose numbers sum to <= a max value.
It takes 3 parameters:
1) a number that'll be the maximum sum of a list of numbers,
2) a function that maps an element of a list to the first element offset in which it occurs.
3) and a list of items.

Examples:

   split_sum 7 [1;2;3];;
   [[1; 2; 3]]

   split_sum 7 [6];;
   [[6]]

   split_sum 7 [1;2;3;4;5;6];;
   [[1; 2; 3]; [4]; [5; 6]]
*)
let split_sum max get_n lst =
  let rec consume_chunk sum (chunk, rest) =
    begin
    match rest with
      | [] -> ([], rest)
      | [x] when sum == 0 -> ([x], [])
      | x :: xs ->
        let n = get_n x xs in
        if sum + n > max
        then ([], x :: xs)
        else (let (c, r) = consume_chunk (sum + n) ([], xs) in (x :: c, r))
    end in
  let rec split xs =
    let (chunk, rest) = consume_chunk 0 ([], xs) in
    match rest with
    | [] -> [chunk]
    | _ -> chunk :: split rest
  in split lst

(*
Based on Pcap_syntax_aux.string_of_pcap_expression
*)
let rec string_of_pe (depth : int) (pcap_e : pcap_expression) : string =
  let string_of_junct depth (pes : pcap_expression list) is_conjunct =
    let indent d = (String.concat "" @@ List.init d (Fun.const "    ")) in
    let is_innermost_junct pes =
      List.for_all (fun p -> Pcap_syntax_aux.size_of_pcap_expression p <= 2) pes
    in
    let junct_s = (if is_conjunct then "&&" else "||") in
    (begin
      match pes with
      | [] -> failwith "Empty " ^ (if is_conjunct then "conjunct" else "disjunct")
      | [pe] ->
          if !Config.relaxed_mode then
            Pcap_syntax_aux.sized_enbracket_pe ~stringifier:(Some (string_of_pe depth)) pe
          else failwith "Singleton " ^ (if is_conjunct then "conjunct" else "disjunct")
      | _ ->
          let result =
            if is_innermost_junct pes
            then (* innermost nested expression *)
              pes
              |> List.map @@ string_of_pe depth
              |> split_sum 40 (fun s rest -> String.length s + if rest == [] then 0 else 4)
              |> List.map @@ String.concat (" " ^ junct_s ^ " ")
              |> String.concat (" " ^ junct_s ^ "\n" ^ (indent depth))
            else (* outer nested expression *)
              pes
              |> List.map @@ string_of_pe (depth + 1)
              |> List.map (fun pe_s -> "(\n" ^ (indent (depth + 1)) ^ pe_s ^ "\n" ^ (indent depth) ^ ")")
              |> String.concat (" " ^ junct_s ^ "\n" ^ (indent depth))
          in
          if !Config.show_brackets_mode then
            "(" ^ result ^ ")"
          else result
    end)
  in
  match pcap_e with
  | Primitive prim -> Pcap_syntax_aux.string_of_primitive prim
  | Atom re -> Pcap_syntax_aux.string_of_rel_expr re
  | And pes -> string_of_junct depth pes true
  | Or pes -> string_of_junct depth pes false
  | Not pe -> "! " ^ Pcap_syntax_aux.sized_enbracket_pe ~stringifier:(Some (string_of_pe depth)) pe
  | True -> "@: True :@"
  | False -> "@: False :@"

let string_of_pcap_expression pe = string_of_pe 0 pe
