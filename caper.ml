(*
  Copyright Nik Sultana, November 2018-December 2022
  Copyright Hyunsuk Bang, December 2022

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Front-end.
*)

let expand_expression (pe : Pcap_syntax.pcap_expression) : Pcap_syntax.pcap_expression =
  pe
(*  |> Pcap_syntax.flatten_pcap_expression*)
  |> Simplifire.flatten
  |> Aux.diag_output_id (fun pe -> "Flattened: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
  |> Expand.disj_expand
  |> Aux.diag_output_id (fun pe -> "Disjunction-expanded: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
  |> (fun pe ->
    if not !Config.unresolved_names_mode then
      pe
      |> Names.resolve_pe
      |> Aux.diag_output_id (fun pe -> "Resolved names: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
    else pe)
  |> Expand.expand_primitives
  |> Aux.diag_output_id (fun pe -> "Expanded primitives: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
  |> Expand.implicit_expand_pe
  |> Aux.diag_output_id (fun pe -> "Expanded implicit clauses: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
(*  |> Pcap_syntax.flatten_pcap_expression*)
  |> Simplifire.flatten
  |> Aux.diag_output_id (fun pe -> "Flattened: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
  |> Contract.deduplicate_pe
  |> Aux.diag_output_id (fun pe -> "Deduplicated: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
  |> Contract.subsumption_pe
  |> Aux.diag_output_id (fun pe -> "Subsumed: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
  |> Simplifire.simplify_pcap
  |> Aux.diag_output_id (fun pe -> "Simplified: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

type general_expression =
  | PCAP of Pcap_syntax.pcap_expression
  | SMT of Pcap_syntax.pcap_expression * Bitvector.bv_formula

let convert_to_string (ge : general_expression) : string =
  match ge with
  | PCAP pe -> (if !Config.parse_and_prettyprint_mode
    then Pretty_printing.string_of_pcap_expression 
    else Pcap_syntax_aux.string_of_pcap_expression) pe
  | SMT (pe, bv) ->
      "; " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n" ^
      Bitvector.string_of_bv_formula bv

let process (parsed_expression : Pcap_syntax.pcap_expression) : general_expression =
  parsed_expression
  |> Aux.diag_output_id (fun pe -> "Starting processing: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

  |> (fun pe ->
    if not !Config.not_expand then
      pe
      |> Aux.diag_output_id (fun pe -> "Expanding expression: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
      |> expand_expression
    else pe)
  |> (fun pe ->
      if not (Pcap_wellformed.check_wellformedness_pe pe) then
        failwith (Aux.error_output_prefix ^ ": " ^ "Failed well-formedness check")
      else pe)
  |> (fun pe ->
    if !Config.smt_translate then
      pe
      |> Aux.diag_output_id (fun pe -> "Translating to SMT: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
      |> Translate.translate_pe
      |> Bitvector_sizing.infer_width_formula
      |> (fun bv -> SMT (pe, bv))
    else PCAP pe)


let main =
  Cli.process_params (Array.to_list Sys.argv);
  if !Config.generate_html_output then Aux.standard_output ~raw:true ("<!--Caper v" ^ Config.version ^ "-->")
  else if not !Config.quiet_mode then Aux.standard_output ("Caper v" ^ Config.version) else ();
  if !Config.show_config_mode then Cli.print_config () else ();
  if not !Config.process_stdin_single_expression_mode && !Config.expression_list = [] then
    Cli.show_usage false
  else if !Config.process_stdin_single_expression_mode && !Config.expression_list <> [] then
    begin
      Aux.error_output "ERR (CLI): Cannot specify both -1stdin and -e\n";
      Cli.show_usage false
    end
  else ();
  if !Config.process_stdin_single_expression_mode then
    Parsing_support.gather_input ()
    |> String.concat " "
    |> Parsing_support.parse_string
    |> process
    |> convert_to_string
    |> Aux.diag_output_id (fun _ -> "Pretty-printed\n")
    |> Aux.standard_output
    |> Aux.diag_output_id (fun _ -> "Done\n")
  else if !Config.expression_list <> [] then
    let expression_count = ref 1 in
    List.iter (fun pe ->
      let processed_e =
        process pe
        |> convert_to_string in
      if List.length (!Config.expression_list) > 1 then
        Aux.standard_output ("; expression " ^ string_of_int !expression_count ^ ":");
      (*FIXME Might be useful to enable this?
        Aux.standard_output ("; " ^ Pcap_syntax_aux.string_of_pcap_expression pe);*)
      Aux.standard_output processed_e;
      expression_count := 1 + !expression_count
    ) !Config.expression_list
  else
    begin
      Aux.error_output "ERR (CLI): Cannot specify both -1stdin and -e\n";
      Cli.show_usage false
    end
