(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Simplification for pcap expressions
*)

open Pcap_syntax

(*FIXME move to Pcap_syntax or Pcap_syntax_aux?*)
let side_effecting_proto (proto : proto) : bool =
  match proto with
  | Vlan
  | Mpls -> true
  | _ -> false
let contains_SE_proto_pe (pe : pcap_expression) : bool =
  let protos =
    Names.protos_in_pe pe Names.StringSet.empty
    |> Names.protos_of_strings in
  Names.ProtoSet.is_empty (Names.ProtoSet.filter side_effecting_proto protos)
  |> not

(*Check for subsumption, for example "ip" and "ip6" don't subsume each other,
  but "ip proto \tcp" subsumes "tcp" -- I implicitly assume that this function
    is called after expansion has been carried out -- i.e., "tcp" has been
    expanded into "ip proto \tcp" for example, which permits us to drop the
    former.  *)
let rec subsumption_pe (pe : pcap_expression) : pcap_expression =
  let subsumes (pe1 : pcap_expression) (pe2 : pcap_expression) : bool =
  match pe1, pe2 with
  | Primitive ((net_proto, None, Some Proto), Escaped_String proto),
    Primitive ((Some other_proto, None, None), Nothing)
    when proto = Pcap_syntax_aux.string_of_proto other_proto -> true
  | _ -> false in
  let filter_subsumed (pes : pcap_expression list) : pcap_expression list =
    List.fold_right (fun (pe : pcap_expression) (acc : pcap_expression list) ->
      let subsumed =
        List.fold_right (fun (pe' : pcap_expression) (acc : pcap_expression option) ->
          if acc <> None then acc
          else if subsumes pe' pe then
            begin
              Aux.diag_output ("INFO: Subsumed: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ " by " ^ Pcap_syntax_aux.string_of_pcap_expression pe' ^ "\n");
              Some pe'
            end
          else None) pes None in
      if subsumed = None then
        begin
(*
          Aux.diag_output ("INFO: Not subsumed: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n");
*)
          (subsumption_pe pe) :: acc
        end
      else acc) pes [] in
  match pe with
  | Primitive _
  | Atom _
  | True
  | False -> pe
  | Not pe' -> Not (subsumption_pe pe')
  | And pes -> And (filter_subsumed pes)
  | Or pes -> Or (filter_subsumed pes)

(*Remove redundant duplicate subformulas -- e.g., turning "A & A" into "A"*)
let rec deduplicate_pe (pe : pcap_expression) : pcap_expression =
  let deduplicate_pes pes =
    List.fold_right (fun pe acc ->
      let pe' = deduplicate_pe pe in
      if List.exists (fun pe ->
        if pe = pe' then
          if not (contains_SE_proto_pe pe) then
            begin
              Aux.diag_output ("INFO: Deduplicating: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n");
              true
            end
          else
            begin
              Aux.diag_output ("INFO: Not deduplicating: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n");
              false
            end
        else false) acc then
          acc
      else pe' :: acc) pes [] in
  match pe with
  | Primitive _
  | Atom _
  | True
  | False -> pe
  | Not pe' -> Not (deduplicate_pe pe')
  | And pes -> And (deduplicate_pes pes)
  | Or pes -> Or (deduplicate_pes pes)
