(*
  Copyright Nik Sultana, November 2018-December 2022
  Copyright Hyunsuk Bang, December 2022

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Support functions
*)

let error_output_prefix = "Caper v" ^ Config.version

let error_output (s : string) : unit =
  Printf.eprintf "%s: %s" error_output_prefix s

let status_output (s : string) : unit =
  if not !Config.quiet_mode then
    error_output s
  else ()

let diag_output (s : string) : unit =
  if !Config.diagnostic_output_mode then
    error_output s
  else ()

let status_output_id (f : 'a -> string) (x : 'a) : 'a =
  status_output (f x);
  x

let diag_output_id (f : 'a -> string) (x : 'a) : 'a =
  diag_output (f x);
  x

let standard_output ?(raw : bool = false) (s : string) : unit =
  let s' =
    if raw then s
    else if !Config.generate_html_output then Html.process_line s else s
  in print_endline s'
