#!/bin/bash
#
# Caper: a pcap expression analysis utility.
# Regression tests for parsing/printing
# Nik Sultana, August 2015

CAPER="$1"
TEST_SCRIPT="$2"

THIS_FILE=`basename "$0"`

function tst() {
  if [ "$#" -ne 2 ]
  then
    echo "Incorrect number of parameters passed to ${THIS_FILE}" >&2
    exit 1
  fi
  ${TEST_SCRIPT} "${CAPER}" "$2" "$1"
}

# These examples of pcap syntax are drawn from
#   https://www.wireshark.org/docs/man-pages/pcap-filter.html

tst "S1" "host foo"
tst "S2" "net 128.3"
tst "S3" "port 20"
tst "S4" "portrange 6000-6008"

tst "S5" "src foo"
tst "S6" "dst net 128.3"
tst "S7" "src or dst port ftp-data"

tst "S8" "ether src foo"
tst "S9" "arp net 128.3"
tst "S10" "tcp port 21"
tst "S11" "udp portrange 7000-7009"
tst "S12" "wlan addr2 0:2:3:4:5:6"
tst "S13" "ether 0:2:3:4:5:6"
tst "S14" "ether host 0:2:3:4:5:6"
tst "S15" "ether src host 0:2:3:4:5:6"
tst "S16" "src foo"
  # means same thing as
#tst "ip src foo or arp src foo or rarp src foo"
tst "S17" "ip src foo || arp src foo || rarp src foo"
tst "S18" "net bar"
  # means same thing as
#test "ip net bar or arp net bar or rarp net bar"
tst "S19" "ip net bar || arp net bar || rarp net bar"
tst "S20" "port 53"
  # means same thing as
#tst "tcp port 53 or udp port 53"
tst "S21" "tcp port 53 || udp port 53"

#tst "host foo and not port ftp and not port ftp-data"
tst "S22" "host foo && ! port ftp && ! port ftp-data"
#tst "tcp dst port ftp or ftp-data or domain"
tst "S23" "tcp dst port ftp || ftp-data || domain"
  # means same thing as
#tst "tcp dst port ftp or tcp dst port ftp-data or tcp dst port domain"
tst "S24" "tcp dst port ftp || tcp dst port ftp-data || tcp dst port domain"

tst "S25" "dst host \host"
tst "S26" "src host \host"
tst "S27" "host \host"
tst "S28" "ip host \host"
#tst "ether proto \ip and host \host"
tst "S29" "ether proto \ip && host \host"
tst "S30" "ether dst ehost"
tst "S31" "ether src ehost"
tst "S32" "ether host ehost"
tst "S33" "gateway \host"
#test "ether host ehost and not host \host"
tst "S34" "ether host ehost && ! host \host"
tst "S35" "dst net \net"
tst "S36" "dst net 192.168.1.0"
tst "S37" "dst net 192.168.1"
tst "S38" "dst net 172.16"
tst "S39" "dst net 10"
tst "S40" "dst net 192.168.1.0 mask 255.255.255.0"
tst "S41" "dst net 192.168.1 mask 255.255.255.0"
tst "S42" "dst net 172.16 mask 255.255.0.0"
tst "S43" "dst net 10 mask 255.0.0.0"
tst "S44" "dst net 192.168.1.0/24"
tst "S45" "dst net 192.168.1/24"
tst "S46" "dst net 172.16/16"
tst "S47" "dst net 10/8"
tst "S48" "dst port 513"
tst "S49" "dst port domain"
tst "S50" "src port \port"
tst "S51" "port \port"
tst "S52" "dst portrange port1-port2"
tst "S53" "src portrange port1-port2"
tst "S54" "portrange port1-port2"
tst "S55" "tcp src port \port"
#tst "less length" # FIXME "length" should be a number, not symbol
tst "S56" "less 10"
#tst "len <= length" # FIXME can "length" be a symbol, or only a number?
tst "S57" "len <= 400"
#tst "greater length" # FIXME "length" should be a number, not symbol
tst "S58" "greater 20"
#tst "len >= length" # FIXME can "length" be a symbol, or only a number?
tst "S59" "len >= 491"
tst "S60" "ip proto protocol"
tst "S61" "ip6 proto protocol"
tst "S62" "ip6 protochain protocol"
tst "S63" "ip6 protochain 6"
tst "S64" "ip protochain protocol"
tst "S65" "ether broadcast"
tst "S66" "ip broadcast"
tst "S67" "ether multicast"
tst "S68" "ether[0] & 1 != 0"
tst "S69" "ip multicast"
tst "S70" "ip6 multicast"
tst "S71" "ether proto protocol"
tst "S72" "fddi protocol arp"
tst "S73" "tr protocol arp"
tst "S74" "wlan protocol arp"
tst "S75" "decnet src host"
tst "S76" "ip[0] & 0xf != 5"
#tst "ip[6:2] & 0x1fff = 0"
tst "S77" "ip[6 : 2] & 0x1fff = 0" #NOTE the test-passed check is whitespace sensitive
#tst "not host vs and ace"
tst "S78" "! host vs && ace"
#tst "not host vs and host ace"
tst "S79" "! host vs && host ace"
#tst "not ( host vs or ace )"
tst "S80" "! (host vs || ace)"

tst "S81" "host sundown"
#tst "host helios and ( hot or ace )"
tst "S82" "host helios && (hot || ace)"
#tst "ip host ace and not helios"
tst "S83" "ip host ace && ! helios"
tst "S84" "net ucb-ether"
#tst "gateway snup and (port ftp or ftp-data)"
tst "S85" "gateway snup && (port ftp || ftp-data)"
#tst "ip and not net localnet"
tst "S86" "ip && ! net localnet"
#tst "tcp[tcpflags] & (tcp-syn|tcp-fin) != 0 and not src and dst net localnet"
#tst "tcp[tcpflags] & (tcp-syn|tcp-fin) != 0 && ! src && dst net localnet"
tst "S87" "tcp[tcpflags] & (tcp-syn | tcp-fin) != 0 && ! src && dst net localnet" #NOTE the test-passed check is whitespace sensitive
#tst "tcp port 80 and (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)"
#tst "tcp port 80 && (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)"
tst "S88" "tcp port 80 && (ip[2 : 2] - (ip[0] & 0xf) << 2) - (tcp[12] & 0xf0) >> 2 != 0" #NOTE the test-passed check is whitespace sensitive
#tst "gateway snup and ip[2:2] > 576"
#tst "gateway snup && ip[2:2] > 576"
tst "S89" "gateway snup && ip[2 : 2] > 576" #NOTE the test-passed check is whitespace sensitive
#tst "ether[0] & 1 = 0 and ip[16] >= 224"
tst "S90" "ether[0] & 1 = 0 && ip[16] >= 224"
#tst "icmp[icmptype] != icmp-echo and icmp[icmptype] != icmp-echoreply"
tst "S91" "icmp[icmptype] != icmp-echo && icmp[icmptype] != icmp-echoreply"

tst "S92" "vlan"
tst "S93" "vlan 100"
tst "S94" "vlan && vlan 100"
