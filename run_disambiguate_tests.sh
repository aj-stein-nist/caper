#!/bin/bash
# Caper: a pcap expression analysis utility.
# Run regression tests wrt disambiguation.
# Nik Sultana, October 2022

# FIXME sensitive to the directory in which this is run
tests/disambiguate.sh "./caper.byte -r -1stdin -q" tests/disambiguate_test.sh
