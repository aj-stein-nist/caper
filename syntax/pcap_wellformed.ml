(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Well-formedness spec for pcap expressions
*)

open Pcap_syntax
open Pcap_syntax_aux
open String_features

(** Well-formedness checking -- e.g., that "vlan" is only applied to a number **)

let check_wellformedness_prim
 ?emit_explanation:(emit_explanation = !Config.diagnostic_output_mode)
 ?emit_warnings:(emit_warnings = not !Config.quiet_mode)
 (prim : primitive) : bool =
  match prim with
  | ((None, None, None), Nothing) ->
    begin
      if emit_explanation then
        Printf.eprintf "ERR: Impossible primitive\n";
      false
    end
  | ((Some net_proto, None, Some Proto), Escaped_String proto) when
      (net_proto = Ip && proto = string_of_proto Icmp6) ||
      (net_proto = Ip6 && proto = string_of_proto Icmp) ->
      begin
        if emit_warnings then
          Printf.eprintf "WARN: Potentially nonsense expression: %s\n" (string_of_primitive prim);
        true
      end
  | ((Some Ether, None, None), Nothing)
  | ((Some Ether, None, None), MAC_Address _) -> true
  | ((Some Ether, Some d, None), Nothing)
  | ((Some Ether, Some d, None), MAC_Address _) when simple_dir d -> true
  | ((Some Ether, _, None), _) ->
     begin
      if emit_explanation then
        Printf.eprintf "ERR: Unsupported ether primitive: '%s'\n" (string_of_primitive prim);
      false
     end
  | ((Some Vlan, None, None), Nothing)
  | ((Some Mpls, None, None), Nothing) ->
      begin
        if emit_warnings then
          Printf.eprintf "WARN: Potentially tricky expression: %s\n" (string_of_primitive prim);
        true
      end
  | ((Some Vlan, None, None), String s)
  | ((Some Mpls, None, None), String s) ->
    if is_unsignedint s then
      begin
        if emit_warnings then
          Printf.eprintf "WARN: Potentially tricky expression: %s\n" (string_of_primitive prim);
        true
      end
    else
      begin
        if emit_explanation then
          Printf.eprintf "ERR: Invalid 'vlan' primitive: %s\n" (string_of_primitive prim);
        false
      end
  | ((Some Vlan, _, _), _)
  | ((Some Mpls, _, _), _) ->
    begin
      if emit_explanation then
        Printf.eprintf "ERR: Invalid 'vlan' primitive: %s\n" (string_of_primitive prim);
      false
    end
  | ((_, _, _), _) ->
      (*FIXME might be too weak*)
      true

let rec check_wellformedness_e
 ?emit_explanation:(emit_explanation = !Config.diagnostic_output_mode)
 ?emit_warnings:(emit_warnings = not !Config.quiet_mode)
 (e : expr) : bool =
  match e with
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len -> true
  | Plus es
  | Times es
  | Binary_And es
  | Binary_Or es
  | Binary_Xor es ->
      List.fold_right (fun e acc ->
        acc && check_wellformedness_e ~emit_explanation ~emit_warnings e) es true
  | Minus (e1, e2)
  | Quotient (e1, e2)
  | Mod (e1, e2)
  | Shift_Right (e1, e2)
  | Shift_Left (e1, e2) ->
      check_wellformedness_e ~emit_explanation ~emit_warnings e1 &&
      check_wellformedness_e ~emit_explanation ~emit_warnings e2
  | Packet_Access (proto_s, e,_) ->
      (*NOTE here check that the protocol supports offsetting -- e.g., not ipv6 in our current implementation*)
      let neg_result = proto_s <> string_of_proto Ether &&
        proto_s <> string_of_proto Ip &&
        proto_s <> string_of_proto Tcp &&
        proto_s <> string_of_proto Udp &&
        proto_s <> string_of_proto Icmp &&
        (*FIXME the protocols that follow aren't supported by the SMT translation*)
        proto_s <> string_of_proto Icmp6 &&
        proto_s <> string_of_proto Ip6 &&
        proto_s <> string_of_proto Arp in
      if not neg_result then
        check_wellformedness_e ~emit_explanation ~emit_warnings e
      else
        begin
          if emit_explanation then
            Printf.eprintf "ERR: Unsupported Packet_Access: '%s'\n" proto_s;
          false
        end

let check_wellformedness_re
 ?emit_explanation:(emit_explanation = !Config.diagnostic_output_mode)
 ?emit_warnings:(emit_warnings = not !Config.quiet_mode)
 (re : rel_expr) : bool =
  match re with
  (*FIXME check for weird expressions e.g., ip[0:1] > 500, since the value of a single byte can never be that big *)
  | Gt (e1, e2)
  | Lt (e1, e2)
  | Geq (e1, e2)
  | Leq (e1, e2)
  | Eq (e1, e2)
  | Neq (e1, e2) ->
      check_wellformedness_e ~emit_explanation ~emit_warnings e1 &&
      check_wellformedness_e ~emit_explanation ~emit_warnings e2 &&
      (*Ensure that each layer is specified only once -- e.g., we
        can't have both ip and ip6*)
      begin
        Names.protos_in_re re Names.StringSet.empty
        |> Names.protos_of_strings
        |> (fun set -> Names.ProtoSet.fold (fun proto acc ->
            (proto, Expand.layer_of_proto proto) :: acc)
            set [])
        |> (fun proto_layers ->
            List.fold_right (fun ((proto, layer) as proto_layer) acc ->
              let collision_proto = ref None in
              if List.exists (fun (proto', layer') ->
                if proto' <> proto && layer' = layer then
                  begin
                    collision_proto := Some proto';
                    true
                  end
                else false) acc then
                  failwith ("Relation '" ^ string_of_rel_expr re ^
                    "' constrains over mutually-exclusive headers: '" ^
                    string_of_proto proto ^ "' and '" ^
                    match !collision_proto with
                    | None -> failwith("Impossible")(*FIXME bad style, nested failwith*)
                    | Some proto' -> string_of_proto proto ^ "'")
              else proto_layer :: acc
        ) proto_layers []);
        true
      end

let rec check_wellformedness_pe
 ?emit_explanation:(emit_explanation = !Config.diagnostic_output_mode)
 ?emit_warnings:(emit_warnings = not !Config.quiet_mode)
 (pe : pcap_expression) : bool =
  match pe with
  | True
  | False -> true
  | Primitive prim ->
      check_wellformedness_prim ~emit_explanation ~emit_warnings prim
  | Atom re -> check_wellformedness_re ~emit_explanation ~emit_warnings re
  | Not pe -> check_wellformedness_pe ~emit_explanation ~emit_warnings pe
  | And pes
  | Or pes ->
      List.fold_right (fun pe acc ->
        acc && check_wellformedness_pe ~emit_explanation ~emit_warnings pe) pes true
