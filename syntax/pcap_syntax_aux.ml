(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Supporting functions for Pcap syntax processing
*)

open Pcap_syntax
open String_features


let string_of_no_bytes = function
  | One -> "1"
  | Two -> "2"
  | Four -> "4"

let rec size_of_expr : expr -> int = function
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len
  | Shift_Right (_, _) (*FIXME take into account size of subexpressions?*)
  | Shift_Left (_, _) (*FIXME take into account size of subexpressions?*)
  | Packet_Access (_, _, _) (*FIXME take into account size of subexpressions?*)
    -> 1
  | Plus es
  | Times es
  | Binary_And es
  | Binary_Or es
  | Binary_Xor es -> List.fold_right (fun e acc -> acc + size_of_expr e) es 1
  | Minus (e1, e2)
  | Quotient (e1, e2)
  | Mod (e1, e2) -> 1 + size_of_expr e1 + size_of_expr e2

let rec sized_enbracket e =
  let e_s = string_of_expr e in
  if size_of_expr e > 1 then "(" ^ e_s ^ ")" else e_s

and string_of_expr = function
  | Identifier s -> s
  | Nat_Literal i -> string_of_int i
  | Hex_Literal v -> v
  | Len -> "len"
  | Plus es ->
    begin
      match es with
      | [] -> failwith "Empty addition"
      | [pe] ->
          if !Config.relaxed_mode then
            sized_enbracket pe
          else failwith "Singleton addition"
      | _ ->
          let result =
            List.map sized_enbracket es
            |> String.concat " + " in
          if !Config.show_brackets_mode then
            "(" ^ result ^ ")"
          else result
    end
  | Minus (e1, e2) ->
    sized_enbracket e1 ^ " - " ^ sized_enbracket e2
  | Times es ->
    List.map sized_enbracket es
    |> String.concat " * "
  | Quotient (e1, e2) ->
    sized_enbracket e1 ^ " / " ^ sized_enbracket e2
  | Mod (e1, e2) ->
    sized_enbracket e1 ^ " % " ^ sized_enbracket e2
  | Binary_And es ->
    begin
      match es with
      | [] -> failwith "Empty binary AND"
      | [pe] ->
          if !Config.relaxed_mode then
            sized_enbracket pe
          else failwith "Singleton binary AND"
      | _ ->
          let result =
            List.map sized_enbracket es
            |> String.concat " & " in
          if !Config.show_brackets_mode then
            "(" ^ result ^ ")"
          else result
    end
  | Binary_Or es ->
    begin
      match es with
      | [] -> failwith "Empty binary OR"
      | [pe] ->
          if !Config.relaxed_mode then
            sized_enbracket pe
          else failwith "Singleton binary OR"
      | _ ->
          let result =
            List.map sized_enbracket es
            |> String.concat " | " in
          if !Config.show_brackets_mode then
            "(" ^ result ^ ")"
          else result
    end
  | Binary_Xor es ->
    begin
      match es with
      | [] -> failwith "Empty binary XOR"
      | [pe] ->
          if !Config.relaxed_mode then
            sized_enbracket pe
          else failwith "Singleton binary XOR"
      | _ ->
          let result =
            List.map sized_enbracket es
            |> String.concat " ^ " in
          if !Config.show_brackets_mode then
            "(" ^ result ^ ")"
          else result
    end
  | Shift_Right (e1, e2) ->
    sized_enbracket e1 ^ " >> " ^ sized_enbracket e2
  | Shift_Left (e1, e2) ->
    sized_enbracket e1 ^ " << " ^ sized_enbracket e2
  | Packet_Access (s, e, nb_opt) ->
    let nb_s =
      match nb_opt with
      | None -> ""
      | Some nb -> " : " ^ string_of_no_bytes nb in
    s ^ "[" ^ string_of_expr e ^ nb_s ^ "]"

let string_of_rel_expr = function
  | Gt (e1, e2) -> sized_enbracket e1 ^ " > " ^ sized_enbracket e2
  | Lt (e1, e2) -> sized_enbracket e1 ^ " < " ^ sized_enbracket e2
  | Geq (e1, e2) -> sized_enbracket e1 ^ " >= " ^ sized_enbracket e2
  | Leq (e1, e2) -> sized_enbracket e1 ^ " <= " ^ sized_enbracket e2
  | Eq (e1, e2) -> sized_enbracket e1 ^ " = " ^ sized_enbracket e2
  | Neq (e1, e2) -> sized_enbracket e1 ^ " != " ^ sized_enbracket e2

let string_of_typ = function
  | Host -> "host"
  | Net -> "net"
  | Port_type -> "port"
  | Portrange -> "portrange"
  | Proto -> "proto"
  | Protochain -> "protochain"
  | Gateway -> "gateway"
  | Less -> "less"
  | Greater -> "greater"

let string_of_dir = function
  | Src -> "src"
  | Dst -> "dst"
  | Src_or_dst -> "src or dst"
  | Src_and_dst -> "src and dst"
  | Inbound -> "inbound"
  | Outbound -> "outbound"
  | Broadcast -> "broadcast"
  | Multicast -> "multicast"

let string_of_proto = function
  | Ether -> "ether"
  | Fddi -> "fddi"
  | Tr -> "tr"
  | Wlan -> "wlan"
  | Ip -> "ip"
  | Ip6 -> "ip6"
  | Arp -> "arp"
  | Rarp -> "rarp"
  | Decnet -> "decnet"
  | Tcp -> "tcp"
  | Udp -> "udp"
  | Sctp -> "sctp"
  | Vlan -> "vlan"
  | Mpls -> "mpls"
  | Icmp -> "icmp"
  | Icmp6 -> "icmp6"

let proto_of_string = function
  | "ether" -> Ether
  | "fddi" -> Fddi
  | "tr" -> Tr
  | "wlan" -> Wlan
  | "ip" -> Ip
  | "ip6" -> Ip6
  | "arp" -> Arp
  | "rarp" -> Rarp
  | "decnet" -> Decnet
  | "tcp" -> Tcp
  | "udp" -> Udp
  | "sctp" -> Sctp
  | "vlan" -> Vlan
  | "mpls" -> Mpls
  | "icmp" -> Icmp
  | "icmp6" -> Icmp6

let string_of_id_atom
 ?debug_output:(debug_output = false)
 (va : value_atom) : string =
  let str_of_escaped_string s = "\\" ^ s in
  let str_of_ipv4_address (i1, i2, i3, i4) =
    string_of_int i1 ^ "." ^
    string_of_int i2 ^ "." ^
    string_of_int i3 ^ "." ^
    string_of_int i4 in
  let str_of_ipv6_address address = address in
  let str_of_ipv4_network (i1, i2_opt, i3_opt, i4_opt, mask_size_opt) =
    let i2s, i3s, i4s, mask_size_s =
      match i2_opt, i3_opt, i4_opt, mask_size_opt with
      | None, None, None, None ->
        "", "", "", ""
      | None, None, None, Some mask_size ->
        "", "", "", "/" ^ string_of_int mask_size
      | Some i2, None, None, None ->
        "." ^ string_of_int i2, "", "", ""
      | Some i2, None, None, Some mask_size ->
        "." ^ string_of_int i2, "", "", "/" ^ string_of_int mask_size
      | Some i2, Some i3, None, None ->
        "." ^ string_of_int i2, "." ^ string_of_int i3, "", ""
      | Some i2, Some i3, None, Some mask_size ->
        "." ^ string_of_int i2, "." ^ string_of_int i3, "", "/" ^ string_of_int mask_size
      | Some i2, Some i3, Some i4, None ->
        "." ^ string_of_int i2, "." ^ string_of_int i3, "." ^ string_of_int i4, ""
      | Some i2, Some i3, Some i4, Some mask_size ->
        "." ^ string_of_int i2, "." ^ string_of_int i3, "." ^ string_of_int i4, "/" ^ string_of_int mask_size
      | _, _, _, _ -> failwith "Unexpected info"(*FIXME give more info*) in
    string_of_int i1 ^ i2s ^ i3s ^ i4s ^ mask_size_s in
  let str_of_ipv4_network_masked (i1, i2_opt, i3_opt, i4_opt, mask) =
    let i2s, i3s, i4s =
      match i2_opt, i3_opt, i4_opt with
      | None, None, None ->
        "", "", ""
      | Some i2, None, None ->
        "." ^ string_of_int i2, "", ""
      | Some i2, Some i3, None ->
        "." ^ string_of_int i2, "." ^ string_of_int i3, ""
      | Some i2, Some i3, Some i4 ->
        "." ^ string_of_int i2, "." ^ string_of_int i3, "." ^ string_of_int i4
      | _, _, _ -> failwith "Unexpected info"(*FIXME give more info*) in
    string_of_int i1 ^ i2s ^ i3s ^ i4s ^ " mask " ^ mask in
  let str_of_ipv6_network (address, mask_length) =
    address ^ "/" ^ mask_length in
  let str_of_mac_address address = address in
  let str_of_port_range (from, until) = from ^ "-" ^ until in
  match va with
  | Nothing ->
      if debug_output then "Nothing" else ""
  | String s ->
      if debug_output then "String " ^ s else s
  | Escaped_String s ->
      let result = str_of_escaped_string s in
      if debug_output then "Escaped_String " ^ result else result
  | IPv4_Address (i1, i2, i3, i4) ->
      let result = str_of_ipv4_address (i1, i2, i3, i4) in
      if debug_output then "IPv4_Address " ^ result else result
  | IPv6_Address address ->
      let result =  str_of_ipv6_address address in
      if debug_output then "IPv6_Address " ^ result else result
  | IPv4_Network (i1, i2_opt, i3_opt, i4_opt, mask_size_opt) ->
      let result = str_of_ipv4_network (i1, i2_opt, i3_opt, i4_opt, mask_size_opt) in
      if debug_output then "IPv4_Network " ^ result else result
  | IPv4_Network_Masked (i1, i2_opt, i3_opt, i4_opt, mask) ->
      let result = str_of_ipv4_network_masked (i1, i2_opt, i3_opt, i4_opt, mask) in
      if debug_output then "IPv4_Network_Masked " ^ result else result
  | IPv6_Network (address, mask_length) ->
      let result = str_of_ipv6_network (address, mask_length) in
      if debug_output then "IPv6_Network " ^ result else result
  | MAC_Address address ->
      let result = str_of_mac_address address in
      if debug_output then "MAC_Address " ^ result else result
  | Port i ->
      let result = i in
      if debug_output then "Port " ^ result else result
  | Port_Range (from, until) ->
      let result = str_of_port_range (from, until) in
      if debug_output then "Port_Range " ^ result else result

let string_of_primitive (((proto_opt, dir_opt, typs_opt), id_atom) : primitive) : string =
  let proto_s =
    match proto_opt with
    | None -> ""
    | Some proto -> string_of_proto proto in
  let dir_s =
    match dir_opt with
    | None -> ""
    | Some dir ->
      (if proto_s <> "" then " " else "") ^ string_of_dir dir in
  let typs_s =
    match typs_opt with
    | None -> ""
    | Some ty ->
      (if proto_s <> "" || dir_s <> "" then " " else "") ^ string_of_typ ty in
  let id_atom_s =
    match string_of_id_atom id_atom with
    | "" -> ""
    | s -> (if proto_s <> "" || dir_s <> "" || typs_s <> "" then " " else "") ^ s
  in proto_s ^ dir_s ^ typs_s ^ id_atom_s

let rec size_of_pcap_expression : pcap_expression -> int = function
  | Primitive _
  | Atom _
  | True
  | False -> 1
  | And pes
  | Or pes ->
    List.fold_right (fun pe acc ->
      acc + size_of_pcap_expression pe) pes 1
  | Not pe -> 1 + size_of_pcap_expression pe

let rec sized_enbracket_pe ?(stringifier :  (pcap_expression -> string) option = None) pe =
  let pe_s =
   match stringifier with
   | None -> string_of_pcap_expression pe
   | Some f -> f pe in
  if size_of_pcap_expression pe > 1 then "(" ^ pe_s ^ ")" else pe_s

and string_of_pcap_expression = function
  | Primitive prim -> string_of_primitive prim
  | Atom re -> string_of_rel_expr re
  | And pes ->
    begin
      match pes with
      | [] -> failwith "Empty conjunct"
      | [pe] ->
          if !Config.relaxed_mode then
            sized_enbracket_pe pe
          else failwith "Singleton conjunct"
      | _ ->
          let result =
            List.map sized_enbracket_pe pes
            |> String.concat " && " in
          if !Config.show_brackets_mode then
            "(" ^ result ^ ")"
          else result
    end
  | Or pes ->
    begin
      match pes with
      | [] -> failwith "Empty disjunct"
      | [pe] ->
          if !Config.relaxed_mode then
            sized_enbracket_pe pe
          else failwith "Singleton disjunct"
      | _ ->
          let result =
            List.map sized_enbracket_pe pes
            |> String.concat " || " in
          if !Config.show_brackets_mode then
            "(" ^ result ^ ")"
          else result
    end
  | Not pe -> "! " ^ sized_enbracket_pe pe
  | True -> "@: True :@"
  | False -> "@: False :@"


let id_K x = x

let primitive_f proto_f dir_f typs_f
 (((proto_opt, dir_opt, typs_opt), va) : primitive) : primitive =
 ((proto_f proto_opt, dir_f dir_opt, typs_f typs_opt), va)

let set_dir dir pid =
  let dir_f dir_opt =
    match dir_opt with
    | None ->
      Some dir
    | Some _ ->
      failwith "'dir' should not be set"(*FIXME give more info*) in
  let _ =
    (*Check to ensure that primitive isn't of an illegal form*)
    match pid with
    | ((_, _, Some typ), _) ->
      begin
        match typ with
        | Proto
        | Protochain
        | Gateway
        | Less
        | Greater -> failwith ("Cannot apply direction to the type '" ^ string_of_typ typ ^ "'")
        | _ -> ()
      end
    | _ -> ()
  in primitive_f id_K dir_f id_K pid

let set_proto proto pid =
  let proto_f proto_opt =
    match proto_opt with
    | None -> Some proto
    | Some _ ->
        failwith ("Cannot set more than one protocol")(*FIXME give more info*)
  in primitive_f proto_f id_K id_K pid

let set_typ typ pid =
  let typ_f typ_opt =
    match typ_opt with
    | None -> Some typ
    | Some _ ->
        failwith ("Cannot set more than one type")(*FIXME give more info*)
  in primitive_f id_K id_K typ_f pid

let set_value v pid =
  match pid with
  | (field, Nothing) -> (field, v)
  | _ -> failwith ("Cannot set more than one value")(*FIXME give more info*)

let free_vars_in_va (va : value_atom) : string list =
  match va with
  | String x
  | Port x ->
      if is_var_name x then [x] else []
  | Or_String xs
  | Or_Port xs -> List.filter is_var_name xs

  | Nothing
  | Escaped_String _
  | IPv4_Address _
  | IPv6_Address _
  | IPv4_Network _
  | IPv4_Network_Masked _
  | IPv6_Network _
  | MAC_Address _
  | Port_Range _
  | Or_Escaped_String _
  | Or_IPv4_Address _
  | Or_IPv6_Address _
  | Or_IPv4_Network _
  | Or_IPv4_Network_Masked _
  | Or_IPv6_Network _
  | Or_MAC_Address _
  | Or_Port_Range _ -> []
let free_vars_in_primitive ((_, va) : primitive) : string list =
  free_vars_in_va va

let is_primitive : pcap_expression -> bool = function
  | Primitive _ -> true
  | _ -> false

 let rec flatten_expr_plus (e : expr) : expr list =
  match e with
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len -> [e]

  | Plus es ->
      List.map flatten_expr_plus es
      |> List.concat

  | Times es -> [Times (List.map flatten_expr es)]
  | Binary_And es -> [Binary_And (List.map flatten_expr es)]
  | Binary_Or es -> [Binary_Or (List.map flatten_expr es)]
  | Binary_Xor es -> [Binary_Xor (List.map flatten_expr es)]

  | Minus (e1, e2) ->
      [Minus (flatten_expr e1, flatten_expr e2)]
  | Quotient (e1, e2) ->
      [Quotient (flatten_expr e1, flatten_expr e2)]
  | Mod (e1, e2) ->
      [Mod (flatten_expr e1, flatten_expr e2)]
  | Shift_Right (e1, e2) ->
      [Shift_Right (flatten_expr e1, flatten_expr e2)]
  | Shift_Left (e1, e2) ->
      [Shift_Left (flatten_expr e1, flatten_expr e2)]

  | Packet_Access (s, e, nb_opt) ->
      [Packet_Access (s, flatten_expr e, nb_opt)]

and flatten_expr_times (e : expr) : expr list =
  match e with
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len -> [e]

  | Plus es -> [Plus (List.map flatten_expr es)]
  | Times es ->
      List.map flatten_expr_times es
      |> List.concat
  | Binary_And es -> [Binary_And (List.map flatten_expr es)]
  | Binary_Or es -> [Binary_Or (List.map flatten_expr es)]
  | Binary_Xor es -> [Binary_Xor (List.map flatten_expr es)]

  | Minus (e1, e2) ->
      [Minus (flatten_expr e1, flatten_expr e2)]
  | Quotient (e1, e2) ->
      [Quotient (flatten_expr e1, flatten_expr e2)]
  | Mod (e1, e2) ->
      [Mod (flatten_expr e1, flatten_expr e2)]
  | Shift_Right (e1, e2) ->
      [Shift_Right (flatten_expr e1, flatten_expr e2)]
  | Shift_Left (e1, e2) ->
      [Shift_Left (flatten_expr e1, flatten_expr e2)]

  | Packet_Access (s, e, nb_opt) ->
      [Packet_Access (s, flatten_expr e, nb_opt)]

and flatten_expr_band (e : expr) : expr list =
  match e with
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len -> [e]

  | Plus es -> [Plus (List.map flatten_expr es)]
  | Times es -> [Times (List.map flatten_expr es)]
  | Binary_And es ->
      List.map flatten_expr_band es
      |> List.concat
  | Binary_Or es -> [Binary_Or (List.map flatten_expr es)]
  | Binary_Xor es -> [Binary_Xor (List.map flatten_expr es)]

  | Minus (e1, e2) ->
      [Minus (flatten_expr e1, flatten_expr e2)]
  | Quotient (e1, e2) ->
      [Quotient (flatten_expr e1, flatten_expr e2)]
  | Mod (e1, e2) ->
      [Mod (flatten_expr e1, flatten_expr e2)]
  | Shift_Right (e1, e2) ->
      [Shift_Right (flatten_expr e1, flatten_expr e2)]
  | Shift_Left (e1, e2) ->
      [Shift_Left (flatten_expr e1, flatten_expr e2)]

  | Packet_Access (s, e, nb_opt) ->
      [Packet_Access (s, flatten_expr e, nb_opt)]

and flatten_expr_bor (e : expr) : expr list =
  match e with
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len -> [e]

  | Plus es -> [Plus (List.map flatten_expr es)]
  | Times es -> [Times (List.map flatten_expr es)]
  | Binary_And es -> [Binary_And (List.map flatten_expr es)]
  | Binary_Or es ->
      List.map flatten_expr_bor es
      |> List.concat
  | Binary_Xor es -> [Binary_Xor (List.map flatten_expr es)]

  | Minus (e1, e2) ->
      [Minus (flatten_expr e1, flatten_expr e2)]
  | Quotient (e1, e2) ->
      [Quotient (flatten_expr e1, flatten_expr e2)]
  | Mod (e1, e2) ->
      [Mod (flatten_expr e1, flatten_expr e2)]
  | Shift_Right (e1, e2) ->
      [Shift_Right (flatten_expr e1, flatten_expr e2)]
  | Shift_Left (e1, e2) ->
      [Shift_Left (flatten_expr e1, flatten_expr e2)]

  | Packet_Access (s, e, nb_opt) ->
      [Packet_Access (s, flatten_expr e, nb_opt)]

and flatten_expr (e : expr) : expr =
  match e with
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len -> e

  | Plus es ->
      Plus
       (List.map flatten_expr_plus es
        |> List.concat)
  | Times es ->
      Times
       (List.map flatten_expr_times es
        |> List.concat)
  | Binary_And es ->
      Binary_And
       (List.map flatten_expr_band es
        |> List.concat)
  | Binary_Or es ->
      Binary_Or
       (List.map flatten_expr_bor es
        |> List.concat)
  | Binary_Xor es ->
      Binary_Xor
       (List.map flatten_expr_bor es
        |> List.concat)

  | Minus (e1, e2) ->
      Minus (flatten_expr e1, flatten_expr e2)
  | Quotient (e1, e2) ->
      Quotient (flatten_expr e1, flatten_expr e2)
  | Mod (e1, e2) ->
      Mod (flatten_expr e1, flatten_expr e2)
  | Shift_Right (e1, e2) ->
      Shift_Right (flatten_expr e1, flatten_expr e2)
  | Shift_Left (e1, e2) ->
      Shift_Left (flatten_expr e1, flatten_expr e2)

  | Packet_Access (s, e, nb_opt) ->
      Packet_Access (s, flatten_expr e, nb_opt)

let flatten_rel_expr (re : rel_expr) : rel_expr =
  match re with
  | Gt (e1, e2) ->
      Gt (flatten_expr e1, flatten_expr e2)
  | Lt (e1, e2) ->
      Lt (flatten_expr e1, flatten_expr e2)
  | Geq (e1, e2) ->
      Geq (flatten_expr e1, flatten_expr e2)
  | Leq (e1, e2) ->
      Leq (flatten_expr e1, flatten_expr e2)
  | Eq (e1, e2) ->
      Eq (flatten_expr e1, flatten_expr e2)
  | Neq (e1, e2) ->
      Neq (flatten_expr e1, flatten_expr e2)

let rec flatten_pcap_expression_conj (e : pcap_expression) : pcap_expression list =
  match e with
  | Primitive _
  | True
  | False -> [e]
  | Atom re -> [Atom (flatten_rel_expr re)]
  | Not e' -> [Not (flatten_pcap_expression e')]
  | And pes ->
      List.map flatten_pcap_expression_conj pes
      |> List.concat
  | Or pes ->
      [Or (List.map flatten_pcap_expression pes)]


and flatten_pcap_expression_disj (e : pcap_expression) : pcap_expression list =
  match e with
  | Primitive _
  | True
  | False -> [e]
  | Atom re -> [Atom (flatten_rel_expr re)]
  | Not e' -> [Not (flatten_pcap_expression e')]
  | And pes ->
      [And (List.map flatten_pcap_expression pes)]
  | Or pes ->
      List.map flatten_pcap_expression_disj pes
      |> List.concat

and flatten_pcap_expression (e : pcap_expression) : pcap_expression =
  match e with
  | Primitive _
  | True
  | False -> e
  | Atom re -> Atom (flatten_rel_expr re)
  | Not e' -> Not (flatten_pcap_expression e')
  | And pes ->
      And (List.map flatten_pcap_expression_conj pes
           |> List.concat)
  | Or pes ->
      Or (List.map flatten_pcap_expression_disj pes
          |> List.concat)

(*Indicates whether an expression includes "side-effecting" expressions, i.e.,
  references to offsets in the packet*)
let rec is_pure_expr (e : expr) : bool =
  match e with
  | Packet_Access _
  | Len ->
      (*This form of expressions make reference to the current packet, so they
        break the congruence of expressions wrt other packets*)
      false
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _ -> true
  | Plus es
  | Times es
  | Binary_And es
  | Binary_Or es
  | Binary_Xor es -> List.fold_right (fun e acc ->
      acc && is_pure_expr e) es true
  | Minus (e1, e2)
  | Quotient (e1, e2)
  | Mod (e1, e2)
  | Shift_Right (e1, e2)
  | Shift_Left (e1, e2) -> is_pure_expr e1 && is_pure_expr e2

let is_pure_expr_re (re : rel_expr) : bool =
  match re with
  | Gt (e1, e2)
  | Lt (e1, e2)
  | Geq (e1, e2)
  | Leq (e1, e2)
  | Eq (e1, e2)
  | Neq (e1, e2) ->
      is_pure_expr e1 && is_pure_expr e2
(*NOTE we could argue that a pcap_expression containing a primitive
       predicate isn't pure, but in this case we're only judging the purity
       of expressions with a pcap_expressions*)
let rec is_pure_expr_pe (pe : pcap_expression) : bool =
  match pe with
  | Primitive _
  | True
  | False -> true
  | Atom re -> is_pure_expr_re re
  | Not pe -> is_pure_expr_pe pe
  | And pes
  | Or pes ->
      List.fold_right (fun pe acc ->
        acc && is_pure_expr_pe pe) pes true
