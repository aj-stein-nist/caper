(*
  Copyright Nik Sultana, November 2018-December 2022
  Copyright Hyunsuk Bang, December 2022

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Configuration
*)

let version = "0.3-dev"

let quiet_mode = ref false
let diagnostic_output_mode = ref false
let parse_and_prettyprint_mode = ref false
let show_config_mode = ref false
let process_stdin_single_expression_mode = ref false
let expression_list : Pcap_syntax.pcap_expression list ref = ref []
let show_brackets_mode = ref false(*FIXME not sure this is actually useful/needed*)
let relaxed_mode = ref false
let unresolved_names_mode = ref false
let not_expand = ref false
let smt_translate = ref false

let generate_html_output = ref false
