(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Simplifier for pcap expressions.
*)

open Pcap_syntax
open Pcap_syntax_aux

(*FIXME move somewhere generic*)
(*Iterate a function until a fixpoint*)
let rec trampoline (f : 'a -> 'a) (x : 'a) : 'a =
  let y = f x in
  if y = x then y
  else trampoline f y

let rec flatten_and (pe : pcap_expression) (acc : pcap_expression list) : pcap_expression list =
  match pe with
  | And pes -> List.fold_right flatten_and pes acc
  | _ -> flatten' pe :: acc
and flatten_or (pe : pcap_expression) (acc : pcap_expression list) : pcap_expression list =
  match pe with
  | Or pes -> List.fold_right flatten_or pes acc
  | _ -> flatten' pe :: acc
and flatten' (pe : pcap_expression) : pcap_expression =
  match pe with
  | And [pe']
  | Or [pe'] -> flatten' pe'
  | And pes -> And (List.fold_right flatten_and pes [])
  | Or pes -> Or (List.fold_right flatten_or pes [])
  | _ -> pe

let rec flatten_plus (e : expr) (acc : expr list) : expr list =
  match e with
  | Plus es -> List.fold_right flatten_plus es acc
  | _ -> flatten_expr' e :: acc
and flatten_times (e : expr) (acc : expr list) : expr list =
  match e with
  | Times es -> List.fold_right flatten_times es acc
  | _ -> flatten_expr' e :: acc
and flatten_band (e : expr) (acc : expr list) : expr list =
  match e with
  | Times es -> List.fold_right flatten_band es acc
  | _ -> flatten_expr' e :: acc
and flatten_bor (e : expr) (acc : expr list) : expr list =
  match e with
  | Times es -> List.fold_right flatten_bor es acc
  | _ -> flatten_expr' e :: acc
and flatten_expr' (e : expr) : expr =
  match e with
  | Plus [e']
  | Times [e']
  | Binary_And [e']
  | Binary_Or [e'] -> flatten_expr' e'
  | Plus es -> Plus (List.fold_right flatten_plus es [])
  | Times es -> Times (List.fold_right flatten_times es [])
  | Binary_And es -> Binary_And (List.fold_right flatten_band es [])
  | Binary_Or es -> Binary_Or (List.fold_right flatten_bor es [])
  | _ -> e

let value_of (e : expr) : int option =
  match e with
  | Nat_Literal i -> Some i
  | Hex_Literal h ->
    let i = Scanf.sscanf h "%u" (fun x -> x) in
    Some i
  | _ -> None

(*FIXME flatten first,
then order according to weight*)
let rec simp_expr (e : expr) : expr =
  match e with
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len -> e

  | Plus [e']
  | Times [e']
  | Binary_And [e']
  | Binary_Or [e'] -> simp_expr e'
  | Minus (e1, e2)
  | Quotient (e1, e2)
  | Shift_Right (e1, e2)
  | Shift_Left (e1, e2) ->
    let semantic =
      match e with
      | Minus (_, _) ->
        fun x y -> x - y
      | Quotient (_, _) ->
        fun x y -> x / y
      | Shift_Right (_, _) ->
        fun x y -> x lsr y
      | Shift_Left (_, _) ->
        fun x y -> x lsl y
      | _ -> failwith (Aux.error_output_prefix ^ ": " ^ "Impossible")
    in
      begin
        match value_of e1, value_of e2 with
        | Some i1, Some i2 ->
          Nat_Literal (semantic i1 i2)
        | _, _ -> e
      end
  | Packet_Access (proto_name, e, no_bytes_opt) ->
    Packet_Access (proto_name, simp_expr e, no_bytes_opt)

let generic_simp_rel_expr (rel_op : expr -> expr -> rel_expr)
 (semantic : int -> int -> pcap_expression) (e1 : expr) (e2 : expr) : pcap_expression =
  let e1' = simp_expr e1 in
  let e2' = simp_expr e2 in
  match (value_of e1', value_of e2') with
  | Some i1, Some i2 -> semantic i1 i2
  | _, _ -> Atom (rel_op e1' e2')

let simp_rel_expr : rel_expr -> pcap_expression = function
  | Gt (e1, e2) ->
(*
    let e1' = simp_expr e1 in
    let e2' = simp_expr e2 in
    match (value_of e1', value_of e2') with
    | Some i1, Some i2 ->
      if i1 > i2 then True else False
    | _, _ -> Atom (Gt (e1', e2'))
*)
    generic_simp_rel_expr (fun e1 e2 -> Gt (e1, e2))
     (fun i1 i2 -> if i1 > i2 then True else False) e1 e2
  | Lt (e1, e2) ->
    generic_simp_rel_expr (fun e1 e2 -> Lt (e1, e2))
     (fun i1 i2 -> if i1 < i2 then True else False) e1 e2
  | Geq (e1, e2) ->
    generic_simp_rel_expr (fun e1 e2 -> Geq (e1, e2))
     (fun i1 i2 -> if i1 >= i2 then True else False) e1 e2
  | Leq (e1, e2) ->
    generic_simp_rel_expr (fun e1 e2 -> Leq (e1, e2))
     (fun i1 i2 -> if i1 <= i2 then True else False) e1 e2
  | Eq (e1, e2) ->
    generic_simp_rel_expr (fun e1 e2 -> Eq (e1, e2))
     (fun i1 i2 -> if i1 = i2 then True else False) e1 e2
  | Neq (e1, e2) ->
    generic_simp_rel_expr (fun e1 e2 -> Neq (e1, e2))
     (fun i1 i2 -> if i1 <> i2 then True else False) e1 e2

let rec simplify_pcap' (pe : pcap_expression) : pcap_expression =
  match pe with
  | And [] -> True
  | Or [] -> False
  | Primitive _ -> pe (*NOTE currently we don't simplify primitives*)
(*  | Atom _ -> pe (*FIXME this might need to expand -- e.g., to require that "ip"*)*)
(*  | Atom re -> simp_rel_expr re*)
  | Atom _ -> pe(*FIXME currently don't simplify atoms*)
  | And pes ->
    And (List.map simplify_pcap' pes)
  | Or pes ->
    Or (List.map simplify_pcap' pes)
  | Not (Not pe) -> simplify_pcap' pe
  | Not pe -> Not (simplify_pcap' pe)
  | True
  | False -> pe

let flatten (pe : pcap_expression) : pcap_expression =
  trampoline flatten' pe
(*
let simplify_pcap (pe : pcap_expression) : pcap_expression =
  trampoline simplify_pcap' pe
*)

(* Simplify conjunctions and disjunctions, filtering out Trues and False as
   sensible, and possibly reducing the subformula to True or False*)
let rec simplify_pcap_connectives (pe : pcap_expression) : pcap_expression =
  match pe with
  | True
  | False
  | Primitive _
  | Atom _ -> pe
  | Not pe -> Not (simplify_pcap_connectives pe)
  | And pes ->
      let falsified = ref false in
      let pes' = List.fold_right (fun pe' acc ->
        if !falsified then []
        else if pe' = True then
          begin
            Aux.diag_output ("INFO: simplify_pcap_connectives : " ^
             Pcap_syntax_aux.string_of_pcap_expression pe ^
             " filtering " ^
             Pcap_syntax_aux.string_of_pcap_expression pe' ^ "\n");
            acc
          end
        else if pe' = False then
          begin
            falsified := true;
            Aux.diag_output ("INFO: simplify_pcap_connectives : " ^
             Pcap_syntax_aux.string_of_pcap_expression pe ^
             " because of " ^
             Pcap_syntax_aux.string_of_pcap_expression pe' ^ "\n");
            []
          end
        else (simplify_pcap_connectives pe') :: acc) pes [] in
      if not !falsified then
        And pes'
      else False
  | Or pes ->
      let verified = ref false in
      let pes' = List.fold_right (fun pe' acc ->
        if !verified then []
        else if pe' = False then
          begin
            Aux.diag_output ("INFO: simplify_pcap_connectives : " ^
             Pcap_syntax_aux.string_of_pcap_expression pe ^
             " filtering " ^
             Pcap_syntax_aux.string_of_pcap_expression pe' ^ "\n");
            acc
          end
        else if pe' = True then
          begin
            verified := true;
            Aux.diag_output ("INFO: simplify_pcap_connectives : " ^
             Pcap_syntax_aux.string_of_pcap_expression pe ^
             " because of " ^
             Pcap_syntax_aux.string_of_pcap_expression pe' ^ "\n");
            []
          end
        else (simplify_pcap_connectives pe') :: acc) pes [] in
      if not !verified then
        Or pes'
      else True

(*Find contradicting subexpressions and simplify any affected connectives.
  For example, "ip && ! ip" is simplified to False,
  as is "ip6 && tcp[0]", and "ip6 && ip".*)
(* FIXME pass an accumulation of conjoined facts *)
let rec simplify_pcap_contradiction (pe : pcap_expression) : pcap_expression =
  match pe with
  | True
  | False
  | Primitive _
  | Atom _ -> pe
  | Not pe -> Not (simplify_pcap_contradiction pe)
  | And pes ->
      let falsified = ref false in
      let pes' = List.fold_right (fun pe acc ->
        let acc' = List.fold_right (fun pe' acc ->
          if !falsified then []
          else
            let newly_falsified =
              match pe' with
              | Atom re ->
                  (*FIXME could check for inconsistent relations*)
                  begin
                    match pe with
                    | Primitive ((Some proto, None, Some Proto), Escaped_String sub_proto) ->
                        begin
                          let protos =
                            Names.protos_in_re re Names.StringSet.empty
                            |> Names.protos_of_strings in
                          (proto = Tcp && (Names.ProtoSet.mem Udp protos || Names.ProtoSet.mem Sctp protos)) or
                          (proto = Udp && (Names.ProtoSet.mem Tcp protos || Names.ProtoSet.mem Sctp protos)) or
                          (proto = Sctp && (Names.ProtoSet.mem Udp protos || Names.ProtoSet.mem Tcp protos)) or
                          (string_of_proto proto = sub_proto) or
                          (sub_proto = string_of_proto Tcp && (Names.ProtoSet.mem Udp protos || Names.ProtoSet.mem Sctp protos)) or
                          (sub_proto = string_of_proto Udp && (Names.ProtoSet.mem Tcp protos || Names.ProtoSet.mem Sctp protos)) or
                          (sub_proto = string_of_proto Sctp && (Names.ProtoSet.mem Udp protos || Names.ProtoSet.mem Tcp protos))
                        end
                    | _ -> false
                  end
              | True
              | False
              | And _
              | Or _ -> pe = Not pe'
              | Not pe'' -> pe = pe''
              | Primitive ((Some proto', None, Some Proto), Escaped_String sub_proto') ->
                  begin
                    match pe with
                    | Primitive ((Some proto, None, Some Proto), Escaped_String sub_proto) ->
                        proto = proto' && sub_proto <> sub_proto'
                    | Atom re ->
                        begin
                          let protos =
                            Names.protos_in_re re Names.StringSet.empty
                            |> Names.protos_of_strings in
                          sub_proto' = string_of_proto Ip6 &&
                            (Names.ProtoSet.mem Tcp protos ||
                            Names.ProtoSet.mem Udp protos ||
                            Names.ProtoSet.mem Sctp protos) (*FIXME might be incomplete*)
                        end
                    | _ -> false
                  end
              | Primitive ((Some proto', _, _), _) ->
                  begin
                    match pe with
                    | Primitive ((Some proto, _, _), _) ->
                        (proto = Ip && proto' = Ip6) || (proto = Ip6 && proto' = Ip)
                  end
              | _ -> false (*FIXME might be too weak*)
            in if newly_falsified then
              begin
                falsified := true;
                Aux.diag_output ("INFO: simplify_pcap_contradiction : " ^
                 Pcap_syntax_aux.string_of_pcap_expression pe ^
                 " and " ^
                 Pcap_syntax_aux.string_of_pcap_expression pe' ^ "\n");
                []
              end
            else pe' :: acc
          ) acc [] in
        if not !falsified then
          pe :: acc
        else []
        ) pes [] in
      if !falsified then False
      else And (List.map simplify_pcap_contradiction pes')
  | Or pes ->
      (*A disjunction can't itself be contradictory (and we check for empty
        disjunctions elsewhere), so we just pass on the processing to subformulas.*)
      Or (List.map simplify_pcap_contradiction pes)

let rec simplify_pcap_singleton_connectives (pe : pcap_expression) : pcap_expression =
  match pe with
  | True
  | False
  | Primitive _
  | Atom _ -> pe
  | Not pe -> Not (simplify_pcap_singleton_connectives pe)
  | And pes ->
      begin
        match List.map simplify_pcap_singleton_connectives pes with
        | [] -> True
        | [pe] -> pe
        | pes -> And pes
      end
  | Or pes ->
      begin
        match List.map simplify_pcap_singleton_connectives pes with
        | [] -> False
        | [pe] -> pe
        | pes -> Or pes
      end

(*Combination of simplification functions*)
let simplify_pcap (pe : pcap_expression) : pcap_expression =
  trampoline (fun pe ->
    simplify_pcap' pe
    |> Aux.diag_output_id (fun pe -> "INFO: simplify_pcap': " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
    |> simplify_pcap_connectives
    |> Aux.diag_output_id (fun pe -> "INFO: simplify_pcap_connectives: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
    |> simplify_pcap_contradiction
    |> Aux.diag_output_id (fun pe -> "INFO: simplify_pcap_contradiction: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
    |> simplify_pcap_singleton_connectives) pe
